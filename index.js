const express = require("express");
const formidable = require("formidable");
var fs = require("fs");

const app = express();
app.use(express.json());

app.get("/", (req, res) => {
  res.send(`
    <form action="/upload" enctype="multipart/form-data" method="post">
      <div>File: <input type="file" name="filetoupload" multiple="multiple" /></div>
      <input type="submit" value="Upload" />
    </form>
  `);
});

app.post("/upload", (req, res, next) => {
  let form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    let oldPath = files.filetoupload.filepath;
    var newPath =
      "D:/Kenneth/Documents/collabera_training/day24/" +
      files.filetoupload.originalFilename;

    fs.readFile(oldPath, function (err, data) {
      if (err) throw err;

      fs.writeFile(newPath, data, function (err) {
        if (err) throw err;
        res.write("File uploaded and move!");
        res.end();
      });
    });
  });
});

app.listen(3000, () => {
  console.log("Server listening on http://localhost:3000 ...");
});
